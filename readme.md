# CheckerDeProxy

A simple java swing program to check a proxy list


### Prerequisites

Use the button "Télécharger les proxy" to download proxy from https://proxyscrape.com/ or use a file containing proxy in the following format :

```
x.x.x.x:x
x.x.x.x:x
...

```
## Preview
![alt text](https://gitlab.com/arnaud-21/proxy-checker/raw/master/apercu.png "CheckerDeProxy preview image") 


## Built With

* [Netbeans](https://fr.netbeans.org/) - IDE 
* [Ant](https://ant.apache.org/) - Dependency Management


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* https://www.developpez.com/
* https://stackoverflow.com/
* java doc
