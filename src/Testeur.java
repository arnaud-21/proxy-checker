import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URL;
import java.util.concurrent.CopyOnWriteArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vherb
 */
public class Testeur extends Thread {
    
    private CopyOnWriteArrayList<Prox> lp;
    private int ind;
    private Prox p;
    public Testeur(CopyOnWriteArrayList<Prox> lp, int i)
    {
        this.lp = lp;
        this.ind = i;
    }
    
    public void run()
    {
        
        try{
            synchronized(lp)
            {this.p= lp.get(ind);}
                SocketAddress addr = new InetSocketAddress(p.getIp(), Integer.parseInt(p.getPort()) );
                Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);
                
                URL url = new URL("http://www.google.com");
                HttpURLConnection uc = (HttpURLConnection)url.openConnection(proxy);
                uc.setConnectTimeout(9000);
                uc.connect();

                //Affichage du res de la requete
                //System.out.println(testliste[i].getIp()+" -> "+uc.getResponseCode() + " : " + uc.getResponseMessage()); 
                if(uc.getResponseCode() == 200)
                {
                   Fenetre.bonprox.add(proxy);
                   System.out.println(proxy.address().toString()); 
                   Fenetre.getArea().append(proxy.address().toString().substring(1)+"\n");
                }
                
                          
        }
        catch (Exception e) {
            System.out.println("Exception pour "+this.ind+" "+e.getMessage());
        }
        
        //Fenetre.c.incr();
        
    }
    
    
    
    
}
