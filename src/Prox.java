
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vherb
 */
public class Prox {
    private String ip;
    private String port;
    private String isok;
    
    public Prox()
    {
        String ip="";
        String port="";
        String isok="";
    }
    
    public Prox(String i, String p, String isok)
    {
        this.ip=i;
        this.port=p;
        this.isok=isok;
    }
    
    public String getIp()
    {
        return this.ip;
    }
    
    public String getPort()
    {
        return this.port;
    }
    
    public String getIsok()
    {
        return this.isok;
    }
    
    public void setIsok(String s)
    {
        this.isok = s;
    }
    
    public String toSring()
    {
        return "IP: "+this.ip+" Port: "+this.port+" isok: "+this.isok;
        
    }
    
  
}
