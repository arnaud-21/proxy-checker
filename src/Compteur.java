
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vherb
 */
public class Compteur extends Thread{
    
    private int compt;
    
    public Compteur()
    {
        this.compt=1;
    }
    
    public synchronized void incr()
    {
        this.compt = compt+1;
    }
    
    
    
    public int getCompt()
    {
        return this.compt;
    }
    
    public void run()
    {
        while(true)
        {
            System.out.println("COMPTEUR="+Fenetre.c.getCompt());
            Fenetre.getPb().setValue(Fenetre.c.getCompt());
            try {
                sleep(1500);
            } catch (InterruptedException ex) {
                Logger.getLogger(Compteur.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}
